# Make rules for FSL projects
#
# Automatic GNU make rules are used for compiling C and C++ files.

all:

help:
	@echo " make         Rebuild project targets";
	@echo " make clean   Remove executables, libraries and object files";
	@echo " make install Install into your local FSLDEVDIR";

clean:
	${RM} -f *.o *.a *.so *.exe depend.mk \
      ${XFILES} ${FXFILES} ${SOFILES} ${AFILES} ${TESTXFILES}

insertcopyright:
	${FSLDIR}/share/fsl/sbin/insertcopyright * */*

depend:
	${RM} -f depend.mk
	${MAKE} depend.mk

# Automatically generate Make rules and dependencies for
# all source files in the project. This creates a file
# called depend.mk which contains a rule for every object
# file to be compiled, which has all header files
# that the source file includes as dependencies. If
# object files are to be saved into a separate build
# directory, the project Makefile should set the BUILDDIR
# variable.
#
# If the NVCC environment variable is set to a nvcc
# compiler, rules are generated for CUDA .cu/.cuh files.
# If CUDA object files are to be saved into a separate
# build directory, the project Makefile should set the
# CUDABUILDDIR variable.
#
# Note that the -MM option is only supported by nvcc in
# CUDA 10.1 and newer, so it is not possible to
# automatically generate dependencies when compiling
# against older CUDA versions.
depend.mk:
	@echo "Building dependency file depend.mk" ; \
	srcfiles=`find . -name "*.c"  -or -name "*.cc"  -or -name "*.cxx" -or -name "*.cpp" -or -name "*.inc" -or -name "*.hpp" -or -name "*.cu" -or -name "*.cuh"` ; \
	for srcfile in $${srcfiles} dummyname; do                                \
	  if [ -f "$${srcfile}" ]; then                                          \
	    srcfile="$${srcfile#./}"                                           ; \
	    prefix="$${srcfile%.*}"                                            ; \
	    basename="$${prefix##*/}"                                          ; \
	    objfile="$${basename}.o"                                           ; \
	    depfile="$${basename}.mk"                                          ; \
	    if [ "$${srcfile%.cu}"  = "$${prefix}" ] ||                          \
	       [ "$${srcfile%.cuh}" = "$${prefix}" ]; then                       \
	      if [ -f "${NVCC}" ]; then                                          \
	        ${NVCC} -MM -MT "${CUDABUILDDIR}$${objfile}" -MF "$${depfile}"   \
	                ${CPPFLAGS} ${INCFLAGS} "$${srcfile}" > /dev/null 2>&1 ; \
	      fi                                                               ; \
	    else                                                                 \
	      ${CC} -MM -MT "${BUILDDIR}$${objfile}" -MF "$${depfile}"           \
	            ${CPPFLAGS} ${INCFLAGS} "$${srcfile}" > /dev/null 2>&1     ; \
	    fi                                                                 ; \
	    if [ -f "$${depfile}" ]; then                                        \
	      cat "$${depfile}" >> depend.mk                                   ; \
	      rm -f "$${depfile}"                                              ; \
	    fi                                                                 ; \
	  else                                                                   \
	    touch depend.mk                                                    ; \
	  fi                                                                   ; \
	done

install:
	@mkdir -p ${FSLDEVDIR}
	@${MAKE} "DESTDIR=${FSLDEVDIR}" master-install-script

master-install-script:
	@if [ "X${PROJNAME}X" = XX ] ; then \
		echo " " ; \
		echo "No PROJNAME defined in the Makefile" ; \
		echo "    ... aborting install" ; \
		echo " " ; \
		exit 4 ; \
	fi;
	@${MAKE} all
	@${MAKE} exeinstall
	@${MAKE} hdrinstall
	@${MAKE} libinstall
	@${MAKE} tclinstall
	@${MAKE} pyinstall
	@${MAKE} datainstall

# Generic routine used to install
# XFILES, PYFILES, SCRIPTS, etc etc
# Expects four arguments:
#  - $1: Label, just used to print a descriptive message
#  - $2: Destination directory for installation
#  - $3: Permission mask to apply to all installed files (e.g. 0755)
#  - $4: Space-separated list of target files/directories to install
define _x_install =
	if ! echo $(wildcard $(4)) | grep -q -e "^ *$$"; then \
		echo Installing $(1) ; \
		${MKDIR} -p -m 0755 $(2) ; \
	fi
	for target in $(4) verylongdummyname ; do \
		if [ -f $$target ] ; then \
			echo ${INSTALL} -m $(3) $$target $(2)/ ; \
			${INSTALL}      -m $(3) $$target $(2)/ ; \
		elif [ -d $$target ]; then \
			echo ${CP} -r $$target $(2)/ ; \
		 	${CP}      -r $$target $(2)/ ; \
		 	find $(2)/`basename $$target` -type d -exec chmod 0755 {} \; ; \
		 	find $(2)/`basename $$target` -type f -exec chmod $(3) {} \; ; \
		fi; \
	done;
endef

# Installs PYFILES into $FSLDIR/etc/fsl/python/$PROJNAME/
pyinstall:
	@${MKDIR} -p -m 0755 $(dest_PYDIR)
	@$(call _x_install,"python scripts",${dest_PYDIR}/${PROJNAME}/,0644,${PYFILES})

# Installs AFILES and SOFILES into $FSLDIR/lib/
libinstall:
	@${MKDIR} -p -m 0755 $(dest_LIBDIR)
	@$(call _x_install,"library files",${dest_LIBDIR},0644,${AFILES} ${SOFILES})

# Installs:
#  - HFILES into $FSLDIR/include/$PROJNAME/
#  - GLOBALHFILES into $FSLDIR/include/
hdrinstall:
	@${MKDIR} -p -m 0755 $(dest_INCDIR)
	@$(call _x_install,"header files",${dest_INCDIR}/${PROJNAME}/,0644,${HFILES})
	@$(call _x_install,"global header files",${dest_INCDIR}/,0644,${GLOBALHFILES})

# Installs DATAFILES into $FSLDIR/data/$PROJNAME/
datainstall:
	@${MKDIR} -p -m 0755 $(dest_DATADIR)
	@$(call _x_install,"data files",${dest_DATADIR}/${PROJNAME}/,0644,${DATAFILES})

# Installs:
#   - XFILES into $FSLDIR/bin/
#   - SCRIPTS into $FSLDIR/bin/
exeinstall:
	@${MKDIR} -p -m 0755 $(dest_BINDIR)
	@$(call _x_install,"binaries",${dest_BINDIR}/,0755,${XFILES})
	@$(call _x_install,"scripts",${dest_BINDIR}/,0755,${SCRIPTS})


# Installs:
#  - TCLFILES into $FSLDIR/tcl/
#  - RUNTCLS into $FSLDIR/bin/
tclinstall:
	@${MKDIR} -p -m 0755 $(dest_TCLDIR)
	@${MKDIR} -p -m 0755 $(dest_BINDIR)
	@$(call _x_install,"tcl scripts",${dest_TCLDIR}/,0755,${TCLFILES})
	@# create RUNTCLS links
	@for lntarget in ${RUNTCLS} verylongdummyname ; do \
		if [ $$lntarget != verylongdummyname ] ; then \
			if [ `uname` = Darwin -o X`uname | grep CYGWIN`X != XX ] ; then \
				lntarget=$${lntarget}_gui ; \
			fi ; \
			cd ${dest_BINDIR} ; ${RM} -f $$lntarget ; \
			ln -s Runtcl $$lntarget ; \
			echo ln -s Runtcl $$lntarget ; \
		fi \
	done

include depend.mk
