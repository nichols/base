#!/usr/bin/env python
"""This script can be used to update a FSL installation to the latest available
FSL release.

This script performs the following tasks:

1. Downloads the FSL release manifest file from the
   ``fsl.installer.fslinstaller.FSL_RELEASE_MANIFEST`` url.

2. Identifies the installed FSL version, and the latest available FSL version.

3. Determines whether an update is possible.

4. Runs "conda env update"
"""


import os.path        as op
import                   os
import                   sys
import                   argparse

import fsl.base.conda as conda
import fsl.installer  as fi


def installed_fsl_version():
    """Reads and returns the contents of ``$FSLDIR/etc/fslversion``. """
    fsldir     = os.environ['FSLDIR']
    fslversion = op.join(fsldir, 'etc', 'fslversion')
    if not op.exists(fslversion):
        return None
    with open(fslversion, 'rt') as f:
        fslversion = f.read()
    return fslversion.strip().split(':')[0]


def do_update(fsldir, oldver, ctx):
    """Runs ``conda env update`` to update the FSL installation.

    :arg fsldir:   Location of FSL installation to update.
    :arg oldver:   Currently installed FSL version.
    :arg ctx:      fslinstaller.Context object, containing information
                   about the new FSL version.
    """

    fi.download_fsl_environment(ctx)

    build    = ctx.build
    newver   = build['version']
    condabin = conda.conda()
    envfile  = ctx.environment_file
    cmd      = f'{condabin} env update -f {envfile} -p {fsldir}'

    # Number of lines of expected output when
    # upgrading from the <installed> version
    # to the new version, used for progress
    # reporting. May or may not be available
    # in the manifest.
    output = build['output'].get(oldver, None)
    if output is not None:
        output = int(output)

    fi.printmsg(f'Updating {fsldir} from version {oldver} to {newver}...',
                fi.INFO)

    # The Context.run method makes sure the
    # shell environment is set up to perform
    # the update correctly.
    ctx.run(fi.Process.monitor_progress, cmd, total=output)
    fi.finalise_installation(ctx)


def main(argv=None):
    """Entry point for ``update_fsl_release``. Determines whether a new version
    of FSL is available and, if possible, updates $FSLDIR to that version.
    """

    if os.getuid() == 0:
        fi.printmsg('Running the update_fsl_release script as root user is '
                    'discouraged! You should run this script as a regular user '
                    '- you will be asked for your administrator password if '
                    'required.', fi.WARNING, fi.EMPHASIS)

    # This script will update the FSL
    # installation at $FSLDIR.  This may
    # not be the same as the environment
    # in which this script is installed,
    # which can be useful for testing/
    # debugging.
    try:
        fsldir = os.environ['FSLDIR']
    except KeyError:
        fi.printmsg('$FSLDIR is not set - aborting', fi.ERROR, fi.EMPHASIS)
        return 1

    # Under normal circumstances, this
    # script is intended to be run without
    # any command-line arguments, However,
    # some hidden/internal options are
    # supported, for debugging, testing,
    # and internal purposes. Refer to the
    # fslinstaller.parse_args function for
    # details.

    # We create a fslinstaller.Context,
    # which handles downloading and
    # parsing the FSL release manifest.
    options = ['username',  'password', 'devrelease',
               'devlatest', 'manifest', 'workdir']
    args    = fi.parse_args(argv, options)
    ctx     = fi.Context(args, fsldir, action='update FSL')

    with fi.tempdir(args.workdir):

        logfile = fi.config_logging('update_fsl_release_', args.workdir)
        fi.printmsg(f'Log file: {logfile}\n', fi.INFO)

        # Retrieve the FSL release build entry,
        # from the manifestand figure out
        # whether an update is viable
        build     = ctx.build
        installed = installed_fsl_version()
        available = build['version']

        # non-official FSL installation?
        if installed is None:
            fi.printmsg('Cannot identify version of FSL installation '
                        f'[{fsldir}]! Aborting update', fi.ERROR, fi.EMPHASIS)
            return 1

        # Already up to date?
        if fi.Version(installed) >= fi.Version(available):
            fi.printmsg(f'Your FSL installation ({fsldir}) is up to date\n'
                        f'  Installed version:        {installed}\n'
                        f'  Latest available version: {available}', fi.INFO)
            return 0

        fi.printmsg('A new version of FSL is available\n', fi.IMPORTANT,
                    '  FSL installation directory: ', fi.EMPHASIS,
                    f'{fsldir}\n',
                    '  Installed version:          ', fi.EMPHASIS,
                    f'{installed}\n',
                    '  Latest available version:   ', fi.EMPHASIS,
                    f'{available}\n', fi.INFO)

        response = fi.prompt('Do you want to update your '
                             'FSL installation? [y/n]:', fi.QUESTION)
        if response.lower() != 'y':
            fi.printmsg('Aborting update', fi.INFO)
            return 0

        do_update(fsldir, installed, ctx)

        fi.printmsg(f'\nFSL [{fsldir}] successfully updated '
                    f'to version {available}', fi.IMPORTANT)


if __name__ == '__main__':
    sys.exit(main())
