#!/usr/bin/env fslpython
#
# This script is used by CUDA-based FSL executables. All FSL CUDA
# executables are named according to the CUDA version they were
# compiled against, according to the format "<exe><CUDA_VER>", e.g.
# "eddy_cuda11.2".
#
# Given a prefix, e.g. "eddy_cuda", this script will identify an
# appropriate "eddy_cudaX.Y" executable to run, given the version of
# CUDA that is installed on the system.


import               glob
import               sys
import               re
import               os
import os.path    as op
import subprocess as sp


def query_cuda_version():
    """Returns the installed CUDA runtime version, as a float, or None
    if CUDA cannot be detected.
    """

    try:
        output = sp.run('nvidia-smi',
                        check=True,
                        text=True,
                        capture_output=True).stdout

    except Exception:
        return None

    cudaver = None
    pat     = r'CUDA Version: (\S+)'
    lines   = output.split('\n')

    for line in lines:
        match = re.search(pat, line)
        if match:
            cudaver = float(match.group(1))
            break

    return cudaver


def find_all_cuda_exes(prefix):
    """Returns a dictionary of {cuda_version : executable} mappings,
    containing all <prefix>X.Y variants that are available.
    """
    cuda_exes = []
    nchars    = len(prefix)
    fsldir    = os.environ['FSLDIR']
    fsldevdir = os.environ.get('FSLDEVDIR', None)

    if fsldevdir is not None:
        cuda_exes.extend(glob.glob(op.join(fsldevdir, 'bin', f'{prefix}*')))

    cuda_exes.extend(glob.glob(op.join(fsldir, 'bin', f'{prefix}*')))

    # Do not consider exact prefix match
    cuda_exes = [e for e in cuda_exes if op.basename(e) != prefix]
    exe_vers  = [float(op.basename(f)[nchars:]) for f in cuda_exes]
    cuda_exes = {ver : exe for ver, exe in zip(exe_vers, cuda_exes)}

    return cuda_exes


def find_suitable_cuda_exe(prefix):
    """Returns the path to a <prefix>X.Y executable which is most
    suitable for running on this machine.
    """

    cuda_ver  = query_cuda_version()
    cuda_exes = find_all_cuda_exes(prefix)
    cuda_vers = list(reversed(sorted(cuda_exes.keys())))

    if cuda_ver is None or len(cuda_exes) == 0:
        return None

    # Find the nearest executable
    # which is less than or equal
    # to the installed cuda
    for exe_ver in cuda_vers:
        if exe_ver <= cuda_ver:
            return cuda_exes[exe_ver]

    # If nvidia-smi reports an
    # older CUDA - than what any of
    # the executables were compiled
    # against, just return the
    # oldest available exe, as the
    # driver might support newer
    # CUDA versions than what is
    # reported.
    return cuda_exes[cuda_vers[-1]]


def main(argv=None):
    """Identify an appropriate variant of a CUDA executable based on the
    variants that are installed, and the CUDA version that is installed.
    The full path to the executable is printed to standard output.

    If CUDA is not available, or a suitable executable cannot be
    identified, nothing is printed.
    """

    if argv is None:
        argv = sys.argv[1:]


    if len(argv) != 1:
        print('Usage: find_cuda_exe <prefix>')
        sys.exit(1)

    if 'FSLDIR' not in os.environ:
        print('FSLDIR is not set - aborting')
        sys.exit(1)

    prefix   = argv[0]
    cuda_exe = find_suitable_cuda_exe(prefix)

    if cuda_exe is not None:
        print(cuda_exe)


if __name__ == '__main__':
    main()
