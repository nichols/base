#!/usr/bin/env python
#
# This script implements the "fslversion" command, which prints information
# about the installed FSL version.
#


import            argparse
import            os
import os.path as op
import            re
import            sys

from typing import List, Tuple, Optional, Sequence

import fsl.base.conda as conda
import fsl.installer  as finst


def read_fslversion(fsldir : str) -> Optional[str]:
    """Returns the contents of the $FSLDIR/etc/fslversion file, or None if
    the file does not exist.
    """
    fslverfile = op.join(fsldir, 'etc', 'fslversion')
    if op.exists(fslverfile):
        with open(fslverfile, 'rt') as f:
            return f.read().strip()
    return None


def get_fsldir_revision_history(fsldir : str) -> List[Tuple[str, str]]:
    """Retrieves and analyses the $FSLDIR conda environment revision history.

    Determines whether this is a "standard" FSL installation (normal
    installation, and all updates through the fsl_update_release command).

    Returns
    """

    # Get conda env revision history, ignoring
    # the first one (see explanation below)
    revs = conda.get_environment_revisions(fsldir)[1:]

    # Search through the conda transaction
    # history to see if this FSL installation
    # has been modified in a non-standard
    # way.
    #
    # Conda revision history for a standard FSL
    # installation should contain revisions
    # of the form:
    #
    #   1. The initial mambaforge installation
    #      (we don't consider this revision).
    #   2. Initial installation of <relA>:
    #      mamba env update -f <fsl-relA>.yml
    #   N. Update to <relN>:
    #      mamba env update -f <fsl-relN>.yml

    platpat = r'(?:linux-64|macos-64|macos-M1)'
    verpat  = r'(?:\d+\.\d+\.\d+)(?:\.\d+)?'
    filepat = rf'fsl-({verpat})_{platpat}.yml'
    pattern = rf'.*(?:conda|mamba)(?: env|-env)? update .*/{filepat}(?: |$)'

    # Mark this installation as "dirty" if
    # there are any commands in the revision
    # history which don't match this pattern.
    dirty = any([re.match(pattern, rev['command']) is None for rev in revs])

    # Extract the date and conda command
    # for each revision, sanitising the
    # commands a little.
    history = []
    for i, rev in enumerate(revs):

        date  = rev['date']
        cmd   = rev['command']
        match = re.match(pattern, cmd)

        # Standard update_fsl_release command?
        if match is not None:
            fslver = match.group(1)
            # Initial installation or update?
            if i == 0: cmd = f'Install FSL {fslver}'
            else:      cmd = f'Update to FSL {fslver}'

        # Some other non-standard command
        # (including update_fsl_package)
        else:
            cmd   = cmd.replace(fsldir, '$FSLDIR/')
            words = list(cmd.split())

            for i, word in enumerate(words):
                if 'tmp' in word:
                    word = op.basename(word)
                words[i] = word

            cmd = ' '.join(words)

        history.append((date, cmd))

    return history, dirty


def print_fslversion(fsldir : str, verbose : bool):
    """Called by main. Prints the FSL version.

    If verbose is True, also prints information about each update to this FSL
    installation.
    """
    fslver      = read_fslversion(fsldir)
    revs, dirty = get_fsldir_revision_history(fsldir)

    # Non-standard installation
    if fslver is None:
        fslver = 'Unknown [$FSLDIR/etc/fslversion does not exist]'

    # Non-standard transactions in $FSLDIR
    # environment revisions
    elif dirty:
        fslver = f'{fslver} (modified)'

    print(f'FSLDIR:  {fsldir}')
    print(f'Version: {fslver}')

    if verbose:
        print('Revisions:')
        for date, cmd in revs:
            print(f'  {date}: {cmd}')


def print_installed_packages(fsldir : str):
    """Called by main if --packages is provided. Prints a list of all FSL
    packages that are installed.
    """
    packages = conda.query_installed_packages(fsldir=fsldir)

    print('Packages:')
    names = []
    vers  = []
    for pkg in packages.values():
        names.append(pkg.name)
        vers .append(pkg.version)

    nlen = max(len(n) for n in names)
    vlen = max(len(v) for v in vers)
    fmt  = f'  {{:{nlen}s}} - {{:{vlen}s}}'

    for name, ver in zip(names, vers):
        print(fmt.format(name, ver))


def create_conda_environment_file(fsldir : str, envfile : str):
    """Called by main if --envfile is provided. Creates a conda environment.yml
    file which fully specifies the FSL installation, using conda env export.
    """
    cmd = f'{conda.conda()} env export -f {envfile} -p {fsldir}'
    finst.Process.check_call(cmd)


def parse_args(args : Sequence[str]) -> argparse.Namespace:
    """Parses command-line arguments. """

    helps = {
        'verbose'  : 'Print additional information.',
        'packages' : 'List all installed FSL packages.',
        'envfile'  : 'Create a conda environment file which fully describes '
                     'this FSL installation.',
    }

    parser = argparse.ArgumentParser(
        'fslversion', description='Report FSL version')
    parser.add_argument('-v', '--verbose',  help=helps['verbose'],
                        action='store_true')
    parser.add_argument('-p', '--packages', help=helps['packages'],
                        action='store_true')
    parser.add_argument('-e', '--envfile',  help=helps['envfile'],
                        metavar='FILE')

    return parser.parse_args(args)


def main(argv : Sequence[str] = None):
    """Main routine for the fslversion command. """

    try:
        fsldir = os.environ['FSLDIR']
    except Exception:
        print('$FSLDIR is not set - aborting')
        return 1

    if argv is None:
        argv = sys.argv[1:]

    args = parse_args(argv)

    print_fslversion(fsldir, args.verbose)
    if args.packages: print_installed_packages(fsldir)
    if args.envfile:  create_conda_environment_file(fsldir, args.envfile)


if __name__ == '__main__':
    sys.exit(main())
