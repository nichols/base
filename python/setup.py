#!/usr/bin/env python


import os.path as op

from setuptools import setup, find_namespace_packages

version = '2302.0'

basedir = op.join(op.dirname(__file__), '..')

with open(op.join(basedir, 'README.md'), 'rt') as f:
    readme = f.read()

setup(
    name='fsl-base',
    version=version,
    description='Base of a FSL installation',
    long_description=readme,
    long_description_content_type='text/markdown',
    url='https://git.fmrib.ox.ac.uk/fsl/base',
    author='Paul McCarthy',
    author_email='paul.mccarthy@ndcn.ox.ac.uk',
    license='Apache License Version 2.0',

    classifiers=[
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
    ],

    # Dependencies are specified in the fsl-base conda
    # recipe at the fsl/conda/fsl-base gitlab repository.
    # install_requires=[],

    packages=['fsl', 'fsl.base'],
    entry_points={
        'console_scripts' : [
            'update_fsl_package = fsl.base.update_fsl_package:main',
            'update_fsl_release = fsl.base.update_fsl_release:main',
            'find_cuda_exe      = fsl.base.find_cuda_exe:main',
            'fslversion         = fsl.base.fslversion:main',
        ]
    }
)
