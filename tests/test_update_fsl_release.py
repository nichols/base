#!/usr/bin/env python
#
# test_update_fsl_release.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path as op
import os
import json
import yaml
import shlex

import fsl.installer               as fi
import fsl.base.conda              as conda
import fsl.base.update_fsl_release as update_fsl_release


def create_dummy_manifest_environment_files(installed, available):

    plat         = fi.identify_platform()
    envfile      = op.abspath(f'fsl-{available}-{plat}.yml')
    manifestfile = 'manifest.json'
    env          = {
        'name' : 'FSL',
        'channels' : [
            'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/',
            'conda-forge'],
        'dependencies' : [
            'fslpy 3.10.0',
            'fsl-base 2205.9',
            'fsl-installer 2.0.1']
    }

    with open(envfile, 'wt') as f:
        f.write(yaml.dump(env, Dumper=yaml.Dumper))

    manifest = {
        'versions' : {
            'latest'   : available,
            available  : [
                {
                    'platform'      : plat,
                    'environment'   : envfile,
                    'sha256'        : fi.sha256(envfile),
                    'base_packages' : ['fsl-base', 'fslpy'],
                    'output'        : {
                        'install'   : '92',
                        installed   : '11'
                    }
                },
            ]
        }
    }

    with open(manifestfile, 'wt') as f:
        f.write(json.dumps(manifest))


def create_test_fsldir(fsldir, fslversion):
    # older versions than in the
    # dummy env file created above
    packages = [
        'python=3.10',
        'fslpy=3.9',
        'fsl-base=2205.7',
        'fsl-installer=2.0.0']
    channels = [
        conda.PUBLIC_FSL_CHANNEL,
        'conda-forge']
    cmd = f'conda create -y -p {fsldir} '                 + \
        ' '.join(f'-c {chan}' for chan in channels) + ' ' + \
-        ' '.join(packages)

    fi.Process.check_call(cmd, print_output=True)
    with open(op.join(fsldir, 'etc', 'fslversion'), 'wt') as f:
        f.write(fslversion)



def test_update_fsl_release():
    installed = '5.0.0'
    available = '7.0.0'
    with fi.tempdir():
        create_test_fsldir('fsl', installed)
        create_dummy_manifest_environment_files(installed, available)

        pkgs = conda.query_installed_packages(True, 'fsl')
        asert pkgs['fslpy']        .version == '3.9.0'
        asert pkgs['fsl-base']     .version == '2205.7'
        asert pkgs['fsl-installer'].version == '2.0.0'

        update_fsl_release.main(shlex.split('--yes --manifest=manifest.json'))

        pkgs = conda.query_installed_packages(True, 'fsl')
        asert pkgs['fslpy']        .version == '3.10.0'
        asert pkgs['fsl-base']     .version == '2205.9'
        asert pkgs['fsl-installer'].version == '2.0.1'


def test_update_fsl_release_already_up_to_date():
    installed = '5.0.0'
    available = '5.0.0'
    with fi.tempdir():
        create_test_fsldir('fsl', installed)
        create_dummy_manifest_environment_files(installed, available)

        pkgs = conda.query_installed_packages(True, 'fsl')
        asert pkgs['fslpy']        .version == '3.9.0'
        asert pkgs['fsl-base']     .version == '2205.7'
        asert pkgs['fsl-installer'].version == '2.0.0'

        update_fsl_release.main(shlex.split('--yes --manifest=manifest.json'))

        pkgs = conda.query_installed_packages(True, 'fsl')
        asert pkgs['fslpy']        .version == '3.9.0'
        asert pkgs['fsl-base']     .version == '2205.7'
        asert pkgs['fsl-installer'].version == '2.0.0'


if __name__ == '__main__':
    test_update_fsl_release()
