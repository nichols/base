#!/usr/bin/env python

import sys
import shutil
import os
import os.path as op

import subprocess as sp
import tempfile


sh_script = """
#!/usr/bin/env {shell}
{source} {initfile}
export PATH=$FSLDIR/share/fsl/bin:$PATH
FSLDIR={fsldir}
PATH=${{FSLDIR}}/share/fsl/bin:${{PATH}}
export FSLDIR PATH
. ${{FSLDIR}}/etc/fslconf/fsl.sh
""".strip()

csh_script = """
#!/usr/bin/env {shell}
{source} {initfile}
# FSL Setup
setenv FSLDIR {fsldir}
setenv PATH ${{FSLDIR}}/share/fsl/bin:${{PATH}}
source ${{FSLDIR}}/etc/fslconf/fsl.csh
""".strip()


# Sanity-check $FSLDIR/etc/fslconf/fsl.sh and the like
def test_fsl_init(fsldir, shell, fslinit=None):

    if fslinit is None:
        fslinit = shell

    initfile = op.join(fsldir, 'etc', 'fslconf', f'fsl.{fslinit}')
    source   = {'sh'   : '.',
                'bash' : '.',
                'tcsh' : 'source',
                'csh'  : 'source'}[shell]

    print(f'running {shell} {initfile}')
    sp.run([shell, initfile], check=True)

    with tempfile.TemporaryDirectory() as td:
        fsldir   = op.join(td, 'fsl')
        etcdir   = op.join(fsldir, 'etc')
        confdir  = op.join(etcdir, 'fslconf')

        os.makedirs(confdir)
        shutil.copy(initfile, confdir)
        sp.run(('touch', op.join(etcdir, 'fslversion')), check=True)

        initfile = op.join(confdir, op.basename(initfile))

        if shell in ('sh', 'bash'): script = sh_script
        else:                       script = csh_script

        scriptfile = op.join(td, 'run')
        script     = script.format(shell=shell,
                                   source=source,
                                   initfile=initfile,
                                   fsldir=fsldir)

        with open(scriptfile, 'wt') as f:
            f.write(script)

        os.chmod(scriptfile, 0o755)

        print(f'running {shell}: {source} {initfile}')
        sp.run([shell, scriptfile], check=True)


if __name__ == '__main__':

    if len(sys.argv) > 1:
        fsldir = sys.argv[1]
    else:
        fsldir = os.environ['FSLDIR']

    test_fsl_init(fsldir, 'bash', 'sh')
    test_fsl_init(fsldir, 'sh')
    test_fsl_init(fsldir, 'tcsh', 'csh')
