#!/usr/bin/env python
#

import os
import sys
import shlex
import os.path as op
import subprocess as sp
import tempfile


def test_make_install():
    basedir = op.join(op.dirname(__file__), '..')
    env = os.environ.copy()
    with tempfile.TemporaryDirectory() as td:

        env['FSLDEVDIR'] = td
        cmd = f'make -C "{basedir}" install'

        print(f'running FSLDEVDIR={td} {cmd}')

        sp.run(shlex.split(cmd), env=env, check=True)

        for dirpath, dirnames, filenames in os.walk(td):
            dirpath = dirpath.replace(td, '${FSLDEVDIR}')
            print(dirpath)
            for filename in filenames:
                print('  ', filename)



if __name__ == '__main__':
    test_make_install()
